FROM golang:1.17.0 as builder

WORKDIR /go/src/gitlab.com/ume-platform-engineering/docker-ingress-verifier


COPY . .

RUN make ingress-verifier-linux

FROM scratch
LABEL MAINTAINER = "Platform Engineering (platform@underwriteme.co.uk)"

ARG git_repository="Unknown"
ARG git_commit="Unknown"
ARG git_branch="Unknown"
ARG built_on="Unknown"

LABEL git.repository=$git_repository
LABEL git.commit=$git_commit
LABEL git.branch=$git_branch
LABEL build.on=$built_on

COPY --from=builder /go/src/gitlab.com/ume-platform-engineering/docker-ingress-verifier/bin/linux/ingress-verifier .

CMD [ "/ingress-verifier" ]
