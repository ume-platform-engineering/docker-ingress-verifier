CURRENT_WORKING_DIR=$(shell pwd)

#------------------------------------------------------------------
# Project build information
#------------------------------------------------------------------
PROJNAME := ingress-verifier
IMAGE := $(PROJNAME):1.0.0

#------------------------------------------------------------------
# Go configuration
#------------------------------------------------------------------
GOCMD := go
GOFMT := gofmt
BIN := bin

#------------------------------------------------------------------
# Build targets
#------------------------------------------------------------------

fmt:
	$(GOFMT) -s -w .

vet:
	$(GOCMD) vet ./...

.PHONY: ingress-verifier-linux
ingress-verifier-linux: fmt vet
	env CGO_ENABLED=0 GOOS=linux GOARCH=amd64 $(GOCMD) build -o $(BIN)/linux/$(PROJNAME)

build:
	docker build -t $(IMAGE) .

scan:
	trivy --version
    ## '--light' option is deprecated and will be removed. See also: https://github.com/aquasecurity/trivy/discussions/1649
    ## cache cleanup is needed when scanning images with the same tags, it does not remove the database
	time trivy image --clear-cache
    ## update vulnerabilities db
	time trivy image --download-db-only
    ## Builds report and puts it in the default workdir $CI_PROJECT_DIR, so `artifacts:` can take it from there
    # time trivy image --ignore-policy trivy-ignore.rego --exit-code 0 --format template --template "@\contrib\html.tpl" --output $(CI_PROJECT_DIR)\trivy-scanning-report.html $(IMAGE)
    # Prints full report
	trivy image --ignore-policy trivy-ignore.rego -s "UNKNOWN,MEDIUM,HIGH,CRITICAL" --exit-code 1 $(IMAGE)

build-and-scan: build scan

build-and-save: build
	mkdir -p image
	docker save $(IMAGE) > image/$(IMAGE).tar

load:
	docker load -i  image/$(IMAGE).tar

load-and-scan: load scan

push: build
	docker login -u $(CI_REGISTRY_USER) -p $(CI_REGISTRY_PASSWORD) $(CI_REGISTRY)
	docker tag $(IMAGE) $(CI_REGISTRY_IMAGE)/$(IMAGE)-$(CI_COMMIT_SHORT_SHA)
	docker push $(CI_REGISTRY_IMAGE)/$(IMAGE)-$(CI_COMMIT_SHORT_SHA)
	docker rmi $(CI_REGISTRY_IMAGE)/$(IMAGE)-$(CI_COMMIT_SHORT_SHA)
	docker logout
